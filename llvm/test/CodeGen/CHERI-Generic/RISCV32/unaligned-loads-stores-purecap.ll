; NOTE: Assertions have been autogenerated by utils/update_test_checks.py UTC_ARGS: --force-update --function-signature --scrub-attributes
; DO NOT EDIT -- This file was generated from test/CodeGen/CHERI-Generic/Inputs/unaligned-loads-stores-purecap.ll
; RUN: %riscv32_cheri_purecap_llc %s -o - | FileCheck %s

; ModuleID = 'global.c'

@a1 = addrspace(200) global i64 0, align 1
@a2 = addrspace(200) global i64 0, align 2
@a4 = addrspace(200) global i64 0, align 4
@a8 = addrspace(200) global i64 0, align 8

define i64 @load_global_i64_align_1(i64 %y) addrspace(200) nounwind {
; CHECK-LABEL: load_global_i64_align_1:
; CHECK:       # %bb.0: # %entry
; CHECK-NEXT:  .LBB0_1: # %entry
; CHECK-NEXT:    # Label of block must be emitted
; CHECK-NEXT:    auipcc ca1, %captab_pcrel_hi(a1)
; CHECK-NEXT:    clc ca1, %pcrel_lo(.LBB0_1)(ca1)
; CHECK-NEXT:    clbu a0, 0(ca1)
; CHECK-NEXT:    clbu a2, 1(ca1)
; CHECK-NEXT:    cincoffset ca3, ca1, 2
; CHECK-NEXT:    clbu a3, 1(ca3)
; CHECK-NEXT:    clbu a4, 2(ca1)
; CHECK-NEXT:    slli a2, a2, 8
; CHECK-NEXT:    or a0, a2, a0
; CHECK-NEXT:    slli a2, a3, 8
; CHECK-NEXT:    or a2, a2, a4
; CHECK-NEXT:    slli a2, a2, 16
; CHECK-NEXT:    or a0, a2, a0
; CHECK-NEXT:    clbu a2, 4(ca1)
; CHECK-NEXT:    cincoffset ca1, ca1, 4
; CHECK-NEXT:    clbu a3, 1(ca1)
; CHECK-NEXT:    cincoffset ca4, ca1, 2
; CHECK-NEXT:    clbu a4, 1(ca4)
; CHECK-NEXT:    clbu a1, 2(ca1)
; CHECK-NEXT:    slli a3, a3, 8
; CHECK-NEXT:    or a2, a3, a2
; CHECK-NEXT:    slli a3, a4, 8
; CHECK-NEXT:    or a1, a3, a1
; CHECK-NEXT:    slli a1, a1, 16
; CHECK-NEXT:    or a1, a1, a2
; CHECK-NEXT:    cret
entry:
  %ret = load i64, i64 addrspace(200)* @a1, align 1
  ret i64 %ret
}

define i64 @load_global_i64_align_2(i64 %y) addrspace(200) nounwind {
; CHECK-LABEL: load_global_i64_align_2:
; CHECK:       # %bb.0: # %entry
; CHECK-NEXT:  .LBB1_1: # %entry
; CHECK-NEXT:    # Label of block must be emitted
; CHECK-NEXT:    auipcc ca0, %captab_pcrel_hi(a2)
; CHECK-NEXT:    clc ca0, %pcrel_lo(.LBB1_1)(ca0)
; CHECK-NEXT:    clhu a1, 0(ca0)
; CHECK-NEXT:    clhu a2, 2(ca0)
; CHECK-NEXT:    cincoffset ca3, ca0, 4
; CHECK-NEXT:    clhu a3, 2(ca3)
; CHECK-NEXT:    clhu a4, 4(ca0)
; CHECK-NEXT:    slli a0, a2, 16
; CHECK-NEXT:    or a0, a0, a1
; CHECK-NEXT:    slli a1, a3, 16
; CHECK-NEXT:    or a1, a1, a4
; CHECK-NEXT:    cret
entry:
  %ret = load i64, i64 addrspace(200)* @a2, align 2
  ret i64 %ret
}

define i64 @load_global_i64_align_4(i64 %y) addrspace(200) nounwind {
; CHECK-LABEL: load_global_i64_align_4:
; CHECK:       # %bb.0: # %entry
; CHECK-NEXT:  .LBB2_1: # %entry
; CHECK-NEXT:    # Label of block must be emitted
; CHECK-NEXT:    auipcc ca1, %captab_pcrel_hi(a4)
; CHECK-NEXT:    clc ca1, %pcrel_lo(.LBB2_1)(ca1)
; CHECK-NEXT:    clw a0, 0(ca1)
; CHECK-NEXT:    clw a1, 4(ca1)
; CHECK-NEXT:    cret
entry:
  %ret = load i64, i64 addrspace(200)* @a4, align 4
  ret i64 %ret
}

define i64 @load_global_i64_align_8(i64 %y) addrspace(200) nounwind {
; CHECK-LABEL: load_global_i64_align_8:
; CHECK:       # %bb.0: # %entry
; CHECK-NEXT:  .LBB3_1: # %entry
; CHECK-NEXT:    # Label of block must be emitted
; CHECK-NEXT:    auipcc ca1, %captab_pcrel_hi(a8)
; CHECK-NEXT:    clc ca1, %pcrel_lo(.LBB3_1)(ca1)
; CHECK-NEXT:    clw a0, 0(ca1)
; CHECK-NEXT:    clw a1, 4(ca1)
; CHECK-NEXT:    cret
entry:
  %ret = load i64, i64 addrspace(200)* @a8, align 8
  ret i64 %ret
}

define void @store_global_i64_align_1(i64 %y) addrspace(200) nounwind {
; CHECK-LABEL: store_global_i64_align_1:
; CHECK:       # %bb.0: # %entry
; CHECK-NEXT:  .LBB4_1: # %entry
; CHECK-NEXT:    # Label of block must be emitted
; CHECK-NEXT:    auipcc ca2, %captab_pcrel_hi(a1)
; CHECK-NEXT:    clc ca2, %pcrel_lo(.LBB4_1)(ca2)
; CHECK-NEXT:    csb a1, 4(ca2)
; CHECK-NEXT:    csb a0, 0(ca2)
; CHECK-NEXT:    cincoffset ca3, ca2, 4
; CHECK-NEXT:    srli a4, a1, 16
; CHECK-NEXT:    csb a4, 2(ca3)
; CHECK-NEXT:    srli a4, a1, 8
; CHECK-NEXT:    csb a4, 1(ca3)
; CHECK-NEXT:    cincoffset ca4, ca2, 2
; CHECK-NEXT:    srli a5, a0, 24
; CHECK-NEXT:    csb a5, 1(ca4)
; CHECK-NEXT:    srli a4, a0, 16
; CHECK-NEXT:    csb a4, 2(ca2)
; CHECK-NEXT:    srli a0, a0, 8
; CHECK-NEXT:    csb a0, 1(ca2)
; CHECK-NEXT:    srli a0, a1, 24
; CHECK-NEXT:    cincoffset ca1, ca3, 2
; CHECK-NEXT:    csb a0, 1(ca1)
; CHECK-NEXT:    cret
entry:
  store i64 %y, i64 addrspace(200)* @a1, align 1
  ret void
}

; Function Attrs: nounwind
define void @store_global_i64_align_2(i64 %y) addrspace(200) nounwind {
; CHECK-LABEL: store_global_i64_align_2:
; CHECK:       # %bb.0: # %entry
; CHECK-NEXT:  .LBB5_1: # %entry
; CHECK-NEXT:    # Label of block must be emitted
; CHECK-NEXT:    auipcc ca2, %captab_pcrel_hi(a2)
; CHECK-NEXT:    clc ca2, %pcrel_lo(.LBB5_1)(ca2)
; CHECK-NEXT:    csh a1, 4(ca2)
; CHECK-NEXT:    csh a0, 0(ca2)
; CHECK-NEXT:    cincoffset ca3, ca2, 4
; CHECK-NEXT:    srli a1, a1, 16
; CHECK-NEXT:    csh a1, 2(ca3)
; CHECK-NEXT:    srli a0, a0, 16
; CHECK-NEXT:    csh a0, 2(ca2)
; CHECK-NEXT:    cret
entry:
  store i64 %y, i64 addrspace(200)* @a2, align 2
  ret void
}

define void @store_global_i64_align_4(i64 %y) addrspace(200) nounwind {
; CHECK-LABEL: store_global_i64_align_4:
; CHECK:       # %bb.0: # %entry
; CHECK-NEXT:  .LBB6_1: # %entry
; CHECK-NEXT:    # Label of block must be emitted
; CHECK-NEXT:    auipcc ca2, %captab_pcrel_hi(a4)
; CHECK-NEXT:    clc ca2, %pcrel_lo(.LBB6_1)(ca2)
; CHECK-NEXT:    csw a1, 4(ca2)
; CHECK-NEXT:    csw a0, 0(ca2)
; CHECK-NEXT:    cret
entry:
  store i64 %y, i64 addrspace(200)* @a4, align 4
  ret void
}

define void @store_global_i64_align_8(i64 %y) addrspace(200) nounwind {
; CHECK-LABEL: store_global_i64_align_8:
; CHECK:       # %bb.0: # %entry
; CHECK-NEXT:  .LBB7_1: # %entry
; CHECK-NEXT:    # Label of block must be emitted
; CHECK-NEXT:    auipcc ca2, %captab_pcrel_hi(a8)
; CHECK-NEXT:    clc ca2, %pcrel_lo(.LBB7_1)(ca2)
; CHECK-NEXT:    csw a1, 4(ca2)
; CHECK-NEXT:    csw a0, 0(ca2)
; CHECK-NEXT:    cret
entry:
  store i64 %y, i64 addrspace(200)* @a8, align 8
  ret void
}
